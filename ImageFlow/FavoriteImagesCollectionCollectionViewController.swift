//
//  FavoriteImagesCollectionCollectionViewController.swift
//  ImageFlow
//
//  Created by Admin on 30.08.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class FavoriteImagesCollectionCollectionViewController: UICollectionViewController {

    var storageRef: FIRStorageReference!
    var ref: FIRDatabaseReference!
    var imageDataForDownload: NSData!
    var posts: [FIRDataSnapshot] = []
    
    private var _refHandle: FIRDatabaseHandle!
    private let leftAndRightPaddings: CGFloat = 20.0
    private let numberOfItemsPerRow: CGFloat = 4.0
    private let heightAdjustment: CGFloat = 0.8

    deinit {
        self.ref.child("favorite/\(FIRAuth.auth()!.currentUser!.uid)").removeObserverWithHandle(_refHandle)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        storageRef = FIRStorage.storage().referenceForURL("gs://image-flow.appspot.com")
        ref = FIRDatabase.database().reference()
        _refHandle = ref.child("favorite/\(FIRAuth.auth()!.currentUser!.uid)").observeEventType(.ChildAdded, withBlock: { (snapshop) in
            self.ref.child("posts/\(snapshop.key)").observeEventType(.Value, withBlock: { (postSnapshot) in
                self.posts.append(postSnapshot)
                self.collectionView!.insertItemsAtIndexPaths([NSIndexPath(forRow: self.posts.count-1, inSection: 0)])
            })
        })
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let width = (CGRectGetWidth(collectionView!.frame) - leftAndRightPaddings) / numberOfItemsPerRow
        let layout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSizeMake(width, width * heightAdjustment)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "toDetailView" {
            let destinationController = segue.destinationViewController as! DetailImageViewController
            let indexPath = collectionView!.indexPathsForSelectedItems()
            let cell =  collectionView!.cellForItemAtIndexPath(indexPath![0]) as! CollectionViewCell
            destinationController.image = cell.image!.image!
            destinationController.post = posts[indexPath![0].row]
        }
    }
 

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return posts.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("mailCell", forIndexPath: indexPath) as! CollectionViewCell
    
        // Configure the cell
        let post = posts[indexPath.row].value as! Dictionary<String, String>
        if let photoUrl = post["photoUrl"] {
            FIRStorage.storage().referenceForURL("gs://image-flow.appspot.com/\(photoUrl)").dataWithMaxSize(INT64_MAX){ (data, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                cell.image!.image = UIImage.init(data: data!)
            }
        }
        return cell
    }

    @IBAction func didTapLogOutAction(sender: AnyObject) {
        do {
            try FIRAuth.auth()!.signOut()
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            performSegueWithIdentifier("fromFavoriteToLogIn", sender: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }

}
