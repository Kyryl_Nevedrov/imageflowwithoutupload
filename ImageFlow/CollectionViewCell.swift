//
//  CollectionViewCell.swift
//  ImageFlow
//
//  Created by Admin on 31.08.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    
}
